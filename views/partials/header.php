<!DOCTYPE html>

<html lang="{{ lang }}">

<head>

<meta charset="UTF-8" /> 



<!-- Bootstrap 3x Include -->
<link href="{{ theme_url }}/assets/css/bootstrap.min.css" rel="stylesheet">

<link href="{{ theme_url }}/assets/css/font-awesome.min.css" rel="stylesheet">

<!-- Hex CSS Includes -->
<link rel="stylesheet" type="text/css" href="{{ theme_url }}/assets/css/hex_css.css" />

<!-- Javascript Includes -->
<script src="{{ theme_url }}/assets/js/jquery.min.js"></script>

{{ template:head }}

<!--[if lt IE 9]>
<script src="{{ theme_url }}/assets/js/html5.js"></script>
<![endif]-->

</head>

<body>

<div style="background-color:#ffffff;">

<img src="assets/files/content_items_files/000002/PETCO-Logo-320-180.png" width="320" height="180" alt="PETCO Logo">

</div>